package com.micropole.msdemo.dao;

import java.util.List;

import com.micropole.msdemo.model.Product;

public interface ProductDao {
	
    public List<Product> findAll();
    public Product findById(int id);
    public Product save(Product product);
}